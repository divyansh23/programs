class Double

    def array_to_be_doubled
        array = [2, 3, 4, 5, 6, 7, 8, 9, "a", "v", "b"]
        print array.map{ |n|  is_numeric?(n) ? n * 2 : n }
    end

    def is_numeric?(num)
        num.is_a?(Fixnum)
    end
end

double1 = Double.new()
double1.array_to_be_doubled